package com.cobesz.cas.findyourcompanion;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import static com.cobesz.cas.findyourcompanion.LocationProvider.TAG;


public class GetContacts extends AsyncTask<Object, Object, ArrayList<HashMap<String, String>>> {

    private WeakReference<MainActivity> mainActivityWeakReference;

    ArrayList<HashMap<String, String>> contactList;

    public GetContacts(MainActivity mainActivity) {
        // bewaar referentie naar main om resultaat terug te sturen
        mainActivityWeakReference = new WeakReference<>(mainActivity);
    }

    @Override
    protected ArrayList<HashMap<String, String>> doInBackground(Object... params) {
        // Haal informatie van internet

        HttpURLConnection con = null;
        Log.d(MapsActivity.LOG_TAG, "Getting a contact");

        try {
            // Build RESTful query for icndb

            URL url = new URL(
                    "http://swapi.co/api/people/");
            con = (HttpURLConnection) url.openConnection();

            con.setReadTimeout(10000 /* milliseconds */);
            con.setConnectTimeout(15000 /* milliseconds */);
            con.setRequestMethod("GET");

            con.setDoInput(true);

            // Start the query
            con.connect();

            // Read results from the query
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "UTF-8"));
            String payload = reader.readLine();
            reader.close();

            contactList = new ArrayList<>();


            // Parse to get translated text
            JSONObject jsonObj = new JSONObject(payload);
            JSONArray contacts = jsonObj.getJSONArray("results");

            for (int i = 0; i < contacts.length(); i++){
                JSONObject c = contacts.getJSONObject(i);
                String name = c.getString("name");
                HashMap<String, String> contact = new HashMap<>();
                contact.put("name", name);


                contactList.add(contact);
            }

//            Log.e(TAG, contactList.toString());

        } catch (IOException e) {
            Log.e(MapsActivity.LOG_TAG, "IOException", e);
        } catch (JSONException e) {
            Log.e(MapsActivity.LOG_TAG, "JSONException", e);
        } catch (Exception e) {
            Log.d(MapsActivity.LOG_TAG, "Something went wrong... ", e);
        } finally {
            if (con != null) {
                con.disconnect();
            }
        }

        // All done



        return contactList;
    }

    // wordt aangeroepen door Android als doInBackground klaar is en uitgevoerd op de UI-thread
    @Override
    protected void onPostExecute(ArrayList a) {
        // stuur resultaat naar de maps activity

//        Log.e(TAG, "In onPostExecute: " + a);
        mainActivityWeakReference.get().setSwapiData(a);

    }
}

