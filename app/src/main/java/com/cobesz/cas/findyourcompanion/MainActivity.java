package com.cobesz.cas.findyourcompanion;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static Button findCompanionButton;

    public ArrayList starwarsGuys;

    private String TAG = MainActivity.class.getSimpleName();

    public MediaPlayer mPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        onClickButtonListener();

        // maak downloadertask aan
        GetContacts task = new GetContacts(this);

        // start task
        task.execute();


        mPlayer = MediaPlayer.create(MainActivity.this, R.raw.music1);
        mPlayer.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.pause_music:
                if (item.isChecked()) item.setChecked(false);
                else item.setChecked(true);
                mPlayer.pause();
                return true;
            case R.id.play_music:
                if (item.isChecked()) item.setChecked(false);
                else item.setChecked(true);
                mPlayer.start();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void onClickButtonListener() {
        findCompanionButton = (Button) findViewById(R.id.button);
        findCompanionButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(MainActivity.this, MapsActivity.class);
                        i.putExtra("swData", starwarsGuys);
                        startActivity(i);
                    }
                }
        );
    }

    public ArrayList setSwapiData(ArrayList a) {

        Log.e(TAG, "In setSwapiData: " + a);

        starwarsGuys = a;

        return starwarsGuys;
    }

    public void onDestroy() {

        mPlayer.stop();
        super.onDestroy();

    }

}


