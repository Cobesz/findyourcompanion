package com.cobesz.cas.findyourcompanion;

import android.app.ProgressDialog;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import static android.support.v7.widget.AppCompatDrawableManager.get;


public class MapsActivity extends FragmentActivity implements
        LocationProvider.LocationCallback,
        OnMapReadyCallback {


//    public ArrayList starwarsGuys;

    public static final String TAG = MapsActivity.class.getSimpleName();

    public final static String LOG_TAG = "Contactlist";

    public Random rand;

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    private LocationProvider mLocationProvider;

    Marker marker;

    private ProgressDialog pDialog;
    private ListView lv;

    public LatLng latLng;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();

        mLocationProvider = new LocationProvider(this, this);


        rand = new Random();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        mLocationProvider.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();

        mLocationProvider.disconnect();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFrag.getMapAsync(this);
        }
    }

    private void setUpMap() {
    }

    public void handleNewLocation(Location location) {

        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        latLng = new LatLng(currentLatitude, currentLongitude);

        if (marker != null) {
            marker.remove();
        }
        marker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("You"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));

        double enemyLat = rand.nextDouble() / 100;
        double enemtLong = rand.nextDouble() / 100;

        LatLng enemyLatLng = new LatLng(enemyLat, enemtLong);


        if(latLng == enemyLatLng) {

            Toast.makeText(MapsActivity.this, "This is my Toast message!",
                    Toast.LENGTH_LONG).show();
        }


    }

    public void placeCompanions(Location location) {

        Bundle b = getIntent().getExtras();

        ArrayList starwarsData = b.getStringArrayList("swData");

        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();

        //markers for Companions
        for (int i = 0; i < starwarsData.size(); i++) {

            String starwarsGuy = starwarsData.get(i).toString().replace("name=", "");

            Log.e(TAG, starwarsGuy);

            //Statement for + range
            if (i < 5) {
                double newLat = rand.nextDouble() / 100;
                double newLong = rand.nextDouble() / 100;

                double swapiLatitude = currentLatitude + newLat;
                double swapiLongitude = currentLongitude + newLong;
                LatLng secondLatLng = new LatLng(swapiLatitude, swapiLongitude);

                marker = mMap.addMarker(new MarkerOptions()
                        .position(secondLatLng)
                        .title(starwarsGuy)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.emblem_jedi_benduflare)));

                if (latLng == secondLatLng) {
                    Toast.makeText(MapsActivity.this, "You've made it to your companion. You're now safe!",
                            Toast.LENGTH_LONG).show();
                }
            }
            //Statement for - range
            else {
                double newLat = -rand.nextDouble() / 100;
                double newLong = -rand.nextDouble() / 100;

                Log.v(TAG, "the Lat value is " + newLat);
                Log.v(TAG, "the Long value is " + newLong);

                double swapiLatitude = currentLatitude + newLat;
                double swapiLongitude = currentLongitude + newLong;
                LatLng secondLatLng = new LatLng(swapiLatitude, swapiLongitude);

                marker = mMap.addMarker(new MarkerOptions()
                        .position(secondLatLng)
                        .title(starwarsGuy)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.emblem_jedi_benduflare)));

                if (latLng == secondLatLng) {
                    Toast.makeText(MapsActivity.this, "You've made it to your companion. You're now safe!",
                    Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setUpMap();
    }
}



